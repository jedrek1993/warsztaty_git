Repozytorium testowe przygotowane na potrzeby warsztatów z systemu kontroli wersji GIT dla Koła Naukowego Robotyków KoNaR.
========================================================

Zadania które należy wykonać:
---------------------------------------------------------
1. Na dzień dobry należy pobrać repozytorium ze zdalnego serwera: 
   >> _git clone_
2. Stwórz nową gałąź, postaraj się o unikalną nazwę ;): 
   >> *git branch nazwa_galęzi*
3. Na gałęzi *master*, w pliku *contributors.txt* dodaj swoje nazwisko, a potem stwórz commit: 
   >> _git add_ 
   >> _git commit_
4. Przejdź na swoją nową gałąź i w pliku *contributors.txt* dodaj swoje imię i nazwisko, a potem dodaj commit: 
   >> *git checkout nazwa_gałezi*
5. Wróć na gałąź *master* i złącz gałęzie, pamiętaj aby rozwiązać wszystkie konflikty: 
   >> _git checkout master_ 
   >> _git merge_
6. W tym momencie można podejrzeć jakimś narzędziem graficznym jak wygląda historia(na linux polecam gitg)
7. Teraz postaraj się żeby historia wyglądała trochę lepiej, zacznijmy od cofnięcia poprzedniego commita:
   >> _git reset HEAD~1_
8. Następnie zmieńmy bazę naszej gałęzi, operację wykonujemy na tej gałęzi:  
   >> *git checkout nazwa_gałezi*  
   >> _git rebase master_
9. Prawdopodobnie będzie trzeba rozwiązać konflikt, o wszystkim poinformuje nas git w terminalu.
10. Teraz przełączamy się na *master* i wykonujemy *merge*. Tym razem uzyskamy ładna, liniową historię zmian.
11. Przyszła pora na wrzucenie naszych commitów na zdalne repozytorium. Dopilnuj, aby wrzucić obie gałęzie: 
   >> _git push_
12. Możliwe, że w międzyczasie ktoś już wrzucił swoje zmiany na repozytorium. W takim wypadku musisz wyrównać sie do nich:
   >> _git pull_ (polecam opcję --rebase)
13. Aby sprawdzić kto i kiedy dodał daną linijkę w pliku wykonajmy polecenie:
   >> _git blame contributors.txt_
14. Oraz sprawdźmy historię wszystkich commitów: 
   >> _git log_
13. Cdn...



Ważne jest aby czytać co git odpowiada na nasze polecenia, ponieważ często daje on nam ciekawe wskazówki. Warto również pamiętać o schowku *git stash* i o tym aby usuwać z niego to co nie jest nam już potrzebne.

To proste ćwiczenie powinno pozwolić wam utrwalić sobie rzeczy o których była mowa na szkoleniu, tak aby w miarę świadomie korzystać z systemu kontroli wersji.

Postarajcie się wykonać je to 10 marca 2016. Wykonanie zadania poprawnie jest niezbędne do uzyskania dostępu do Konarowego repozytorium ;)

Do końca tygodnia wrzucę jeszcze jedno zadanie dla wszystkich którzy będą chcieli poćwiczyć obsługę git'a.


Porady i sztuczki:
---------------------------------------------------------

[Bardzo fajny tutorial do poczytania o pracy z gitem](https://www.atlassian.com/git/tutorials/)

[Spis przydatnych komend](https://github.com/arslanbilal/git-cheat-sheet)


Aby wyświetlić nazwę gałęzi na której się znajdujemy w terminalu, do pliku ~/.bashrc należy dodać na końcu:

    :::bash
    function be_get_branch {
      local dir="$PWD"
      local vcs
      local nick
      while [[ "$dir" != "/" ]]; do
        for vcs in git hg svn bzr; do
          if [[ -d "$dir/.$vcs" ]] && hash "$vcs" &>/dev/null; then
            case "$vcs" in
              git) __git_ps1 "${1:-(%s) }"; return;;
              hg) nick=$(hg branch 2>/dev/null);;
              svn) nick=$(svn info 2>/dev/null\
                    | grep -e '^Repository Root:'\
                    | sed -e 's#.*/##');;
              bzr)
                local conf="${dir}/.bzr/branch/branch.conf" # normal branch
                [[ -f "$conf" ]] && nick=$(grep -E '^nickname =' "$conf" | cut -d' ' -f 3)
                conf="${dir}/.bzr/branch/location" # colo/lightweight branch
                [[ -z "$nick" ]] && [[ -f "$conf" ]] && nick="$(basename "$(< $conf)")"
               [[ -z "$nick" ]] && nick="$(basename "$(readlink -f "$dir")")";;
           esac
           [[ -n "$nick" ]] && printf "${1:-(%s) }" "$nick"
           return 0
         fi
       done
       dir="$(dirname "$dir")"
     done
    }

    export PS1="\[\e[01;33m\]\u@\h\[\e[0m\]\[\e[00;37m\]:\[\e[0m\]\[\e[01;34m\]\w\[\e[0m\]\[\e[00;37m\] \[\e[0m\]\[\e[01;31m\][ \t\[\e[0m\]\[\e[00;37m\] \[\e[0m\]\[\e[01;31m\]]\[\e[0m\]"

    export GIT_PS1_SHOWDIRTYSTATE=yes
    export PS1="${PS1} \$(be_get_branch "$2")$ ";